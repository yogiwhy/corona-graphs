new Vue({
    i18n,
    el: '#app',
    components: {
        apexchart: VueApexCharts,
        sickchart,
        testchart,
        rchart
    },
    methods: {
        changeCountry(event) {
            var name = event.target.options[event.target.options.selectedIndex].text;
            this.selCountry = name
        },
        changeLanguage(event) {
            var name = event.target.options[event.target.options.selectedIndex].text;
            this.selLanguage = name
            console.log(this.selLanguage)
        }
    },
    data: {
        countries: [
            { name: 'Germany' },
            // { name: 'United States' },
        ],
        selCountry: "Germany",
        languages: ['de', 'en'],
        selLanguage: "English",
    },

})